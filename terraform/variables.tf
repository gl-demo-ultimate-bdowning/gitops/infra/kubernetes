variable "aws_region" {
  type        = string
  description = "The AWS region to deploy resources in."
  default     = "us-east-1"
}

variable "ci_project_namespace" {
  type        = string
  description = "Group Path the current project resides within used to register the GitLab Agent"
}

variable "cluster_name" {
  type        = string
  description = "Name of the EKS cluster."
  default     = "gitlab-gitops-eks"
}

variable "cluster_version" {
  type        = string
  description = "EKS cluster version."
}

variable "ci_project_id" {
  type        = string
  description = "Current project ID used to bootstrap flux"
}

variable "ci_project_namespace_id" {
  type        = number
  description = "Group ID the current project resides within used to deploy Runner"
}

variable "flux_namespace" {
  type        = string
  description = "Namespace where Flux will be installed."
  default     = "flux-system"
}

variable "network_name" {
  type        = string
  description = "VPC Name for network resource"
  default     = "gitlab-gitops-vpc"
}

variable "gitlab_token" {
  type        = string
}