terraform {
  required_version = ">= 1.3.0"
  required_providers {
    aws  = {
      source  = "hashicorp/aws"
      version = "~> 5.81"
    }
    flux = {
      source  = "fluxcd/flux"
      version = ">= 1.2"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 17.6"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 4.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.35"
    }
  }
}