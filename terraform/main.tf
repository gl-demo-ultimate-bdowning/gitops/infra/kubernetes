data "aws_availability_zones" "available" {
  # Exclude local zones
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

locals {
  vpc_cidr = "10.0.0.0/16"
  azs      = slice(data.aws_availability_zones.available.names, 0, 3)
}

################################################################################
# EKS Module Deploy
################################################################################

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 20.0"

  cluster_name                   = var.cluster_name
  cluster_version                = var.cluster_version
  cluster_endpoint_public_access = true

  enable_cluster_creator_admin_permissions = true

  cluster_compute_config = {
    enabled    = true
    node_pools = ["general-purpose"]
  }

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

}

data "aws_eks_cluster" "eks" {
  depends_on = [module.eks]
  name = var.cluster_name
}

data "aws_eks_cluster_auth" "eks" {
  depends_on = [module.eks]
  name = var.cluster_name
}

################################################################################
# VPC
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.0"

  name = var.network_name
  cidr = local.vpc_cidr

  azs             = local.azs
  private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 4, k)]
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 48)]
  intra_subnets   = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 52)]

  enable_nat_gateway = true
  single_nat_gateway = true

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/role/internal-elb" = 1
  }

}

################################################################################
# GitLab Objects - Deploy Token for Flux and Project Runner
################################################################################

data "gitlab_project" "current-project" {
  id = var.ci_project_id
}

data "gitlab_group" "current-group-runner" {
  group_id = var.ci_project_namespace_id
}

resource "tls_private_key" "flux" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

resource "gitlab_deploy_key" "gitlab-gitops-eks-flux-deploy-token" {
  project  = data.gitlab_project.current-project.path_with_namespace
  title    = "Flux"
  key      = tls_private_key.flux.public_key_openssh
  can_push = true
}

resource "gitlab_user_runner" "gitops-group-runner" {
  runner_type = "group_type"
  group_id  = data.gitlab_group.current-group-runner.group_id

  tag_list    = ["tf-runner"]
  description = "A runner created using a user access token instead of a registration token"
  untagged    = true
}

resource "gitlab_cluster_agent" "gitlab-gitops-eks-agent" {
  project = data.gitlab_project.current-project.id
  name    = "gitlab-gitops-eks-agent"
}

resource "gitlab_cluster_agent_token" "gitlab-gitops-eks-agent-token" {
  project     = data.gitlab_project.current-project.id
  agent_id    = gitlab_cluster_agent.gitlab-gitops-eks-agent.agent_id
  name        = "gitlab-gitops-eks-agent-token"
  description = "Token for the gitlab-gitops-eks-agent used with `gitlab-agent` Helm Chart"
}

resource "gitlab_repository_file" "gitlab-gitops-eks-agent-config" {
  project   = data.gitlab_project.current-project.id
  branch    = "main"
  file_path = ".gitlab/agents/${gitlab_cluster_agent.gitlab-gitops-eks-agent.name}/config.yaml"
  content = base64encode(<<DOC
# GitLab Agent for Kubernetes Configuration
user_access:
  access_as:
    agent: {}
  groups:
    - id: ${var.ci_project_namespace}
DOC
  )
  author_email   = "terraform@pipeline.run"
  author_name    = "Terraform"
  commit_message = "feature: add agent config for ${gitlab_cluster_agent.gitlab-gitops-eks-agent.name} [skip ci]"
}

#####################################################################################
# Bootstrap Flux to GitLab Project and Deploy to Cluster. Project defined in Provider
#####################################################################################

resource "flux_bootstrap_git" "flux-eks-bootstrap" {
  depends_on = [gitlab_deploy_key.gitlab-gitops-eks-flux-deploy-token]

  embedded_manifests = true
  path               = "flux/eks"
}

################################################################################
# K8s Objects - Secrets for Agent and Runner
################################################################################

resource "kubernetes_secret" "gitlab-gitops-eks-agent" {
  depends_on = [flux_bootstrap_git.flux-eks-bootstrap]
  metadata {
    name = "gitlab-gitops-eks-agent"
    namespace = "flux-system"
  }

  data = {
    token           = gitlab_cluster_agent_token.gitlab-gitops-eks-agent-token.token
  }

  type = "Opaque"
}

resource "kubernetes_secret" "gitlab-gitops-eks-runner" {
  depends_on = [flux_bootstrap_git.flux-eks-bootstrap]
  metadata {
    name = "gitlab-gitops-eks-runner"
    namespace = "flux-system"
  }

  data = {
    runner-registration-token = ""
    runner-token              = gitlab_user_runner.gitops-group-runner.token
  }

  type = "Opaque"
}