# Kubernetes GitOps Demonstration Project
## What Does This Project Accomplish?
- ### Deploys Environment Runtime for GitOps Workflow
    - Terraform Deploys:
        - VPC on AWS
        - EKS Cluster in Auto-Mode on AWS
        - GitLab Deploy Token
        - GitLab Runner and Associates it with Group this project resides within and gives it a tag of `tf-runner`
        - GitLab Agent for Kubernetes and registers within this project
        - GitLab Agent for Kubernetes configurations file within this project to share the agent with other projects in the same group
        - Bootstraps Flux into the EKS cluster and points it back to this project for self-management and deployment of subsequent manifests
        - Kubernetes Secrets to support Runner and Agent registration
        - GitLab Environment mapped to flux-system namespace to show pods running in the deployed cluster. Pods running should include:
            - Flux Components (various controllers)
            - GitLab Agent for Kubernetes (x2)
            - GitLab Runner Manager

## How to Deploy this Project
### Fork this Project
- Fork this project to your own environment. It will create a group runner in the group the project resides in. If you'd like to have a second 'developer' project to use the Kubernetes cluster, you'll need to create that separately. This project can be used as a reference - [gl-demo-ultimate-bdowning-goweb](https://gitlab.com/gl-demo-ultimate-bdowning/gitops/app/goweb)
### Setting CICD Variables
- This Project deploys AWS and GitLab objects, therefore you'll need to provide access via CICD Varibales.
1. Create AWS CICD Variables
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_DEFAULT_REGION`
2. Create GitLab Group Token
- `GITLAB_TOKEN_GROUP`

### Run the Pipeline
- The pipeline should take roughly 10-15minutes - the bulk of the time is spent deploying the eks cluster
- You can run this in an MR workflow, or directly on `main`. The deploy job, or `terraform apply`, is a manual job so you'll need to watch the pipeline to start that job after the `validate` and `plan` stages.

### Verification
- Check the `Operate > environments` section for `flux-system-namespace` environment and ensure the Flux kustomization is reconciled correctly, and all the pods are running. 
- Check the agent status in `Operate > Kubernetes Clusters` in both the source project and the target project to ensure it has a successful connection
- Check the group `Build > Runners` section to ensure the runner is registered successfully. 

### Cleanup
- Go to the most recent successful build pipeline and run the `destroy` job to clean up all deployed resources. Then, run the `delete-state` job to clear the terraform backend state currently stored in `Operate > Terraform States`


